FROM mariadb

USER 0
COPY db-optimize.sh /
RUN chmod a+x /db-optimize.sh

USER 1001

ENTRYPOINT ["sh", "-c", "/db-optimize.sh"]