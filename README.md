# docker-database-optimize
Contains a docker container that runs a script to optimize mysql tables.
This script relies on injected environment variables to define which credentials to use and on which database it should connect.

Usage in docker:

    docker run -e"DATABASE_USER=user" -e"DATABASE_PASSWORD=password" -e"DATABASE_HOST=127.0.0.1" -e"DATABASE_NAME=database-name registry.gitlab.com/xrow-public/docker-database-optimize"

Usage in kubernetes:
 
 

    kind: CronJob
    apiVersion: batch/v1beta1
    metadata:
      name: database-optimize
    spec:
      schedule: 0 6 * * 6
      suspend: false
      jobTemplate:
        spec:
          template:
            metadata:
              labels:
                name: database-optimize
            spec:
              containers:
                - name: database-optimize
                  image: rregistry.gitlab.com/xrow-public/docker-database-optimize:latest
                  env:
                    - name: DATABASE_USER
                      value: user
                    - name: DATABASE_PASSWORD
                      value: password
                    - name: DATABASE_HOST
                      value: 127.0.0.1
                    - name: DATABASE_NAME
                      value: database-name
              restartPolicy: OnFailure
              terminationGracePeriodSeconds: 30
      successfulJobsHistoryLimit: 3
      failedJobsHistoryLimit: 1


