#!/bin/bash

if [ -z ${DATABASE_USER+x} ]
then
    echo "No database user specified!"
    exit
fi
if [ -z ${DATABASE_PASSWORD+x} ]
then
    echo "No database password specified!"
    exit
fi
if [ -z ${DATABASE_NAME+x} ]
then
    echo "No database name specified!"
    exit
fi
if [ -z ${DATABASE_HOST+x} ]
then
    echo "No database host specified!"
    exit
fi

mysql="mysql -u $DATABASE_USER -p$DATABASE_PASSWORD -h$DATABASE_HOST $DATABASE_NAME"

tables=$($mysql -B -e'Show tables;')

for table in $tables
do
  $mysql -e"optimize table $table"
done
